CONF_FILES = bash_profile bashrc gitconfig

all :: 
	@echo "Type 'make install' if you want to create symlinks"

install :: 
	$(foreach f,$(CONF_FILES), test -f ~/.$(f) && mv ~/.$(f) ~/.$(f).old; ln -vs etc/$(f) ~/.$(f) ; )

uninstall ::
	$(foreach f,$(CONF_FILES), test -L ~/.$(f) && rm ~/.$(f); cp -vn ~/.$(f).old ~/.$(f);)
