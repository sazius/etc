# -*- mode: sh -*-

umask 022

export PATH=~/bin:~/.local/bin:$PATH

# Stop if we are not really interactive
test ! -t 0 && return

# -----------------------------------------------------------------------------

# Set default applications
if ! [ -x "$(command -v emacsclient)" ]; then
    export EDITOR="vim"
else
    export EDITOR="emacsclient -a vim"
fi
export BROWSER=firefox

# Enable bash completion in interactive shells
if ! shopt -oq posix; then
 if [ -f /usr/share/bash-completion/bash_completion ]; then
   . /usr/share/bash-completion/bash_completion
 elif [ -f /etc/bash_completion ]; then
   . /etc/bash_completion
 fi
fi

# Handy calculator.
calc() {
  awk "BEGIN{ print $* }" ;
}


# -----------------------------------------------------------------------------
# History setup
# -----------------------------------------------------------------------------

# lines to save from current session
export HISTSIZE=1048576

# total lines of file
export HISTFILESIZE=1048576

# ignore duplicates, those beginning with space
export HISTCONTROL=ignoreboth

# Print the timestamp of each command
export HISTTIMEFORMAT='%F %T '

# append to history, not overwrite
shopt -s histappend

# attempt to save all lines of a multiple-line command in the same entry
shopt -s cmdhist

# append to history at each prompt
export PROMPT_COMMAND='history -a'


# -----------------------------------------------------------------------------
# Fancy bash options
# -----------------------------------------------------------------------------

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# enable ** expansion
shopt -s globstar

# "safe" paste - does not run pasted commands without actually pressing enter
bind 'set enable-bracketed-paste on'

# -----------------------------------------------------------------------------
# Set prompt
# -----------------------------------------------------------------------------

BOLD="\[$(tput bold)\]"
RESET="\[$(tput sgr0)\]"
BLUE="\[$(tput setaf 4)\]"
YELLOW="\[$(tput setaf 3)\]"

export PS1="${BLUE}${BOLD}\h${RESET}${BLUE}:\w ${YELLOW}\$ ${RESET}"

#export PROMPT_COMMAND="echo -ne '\033]0;${HOSTNAME}\007'; $PROMPT_COMMAND"
export PROMPT_COMMAND="echo -ne '\E]0;${HOSTNAME}\a'; $PROMPT_COMMAND"


# -----------------------------------------------------------------------------
# Aliases 
# -----------------------------------------------------------------------------

alias ls='ls -F --color=auto'
alias ll='ls -l'
alias lt='ls -ltr'
alias la='ls -latr'

alias grep='grep --color=auto'

alias dff='df -h -x tmpfs -x devtmpfs'

alias e='emacsclient -n -a vim'
alias m='zless'
alias psu='ps f -U $USER -o pid,nice,tty,vsize,args'

# Directory stack
alias P='pushd'
alias p='popd'
alias d='dirs -v'

# Fix ssh in old tmux
alias tmux_fix_ssh='export $(tmux show-environment SSH_AUTH_SOCK)'
alias tmuxa='tmux new -As $(hostname -s)'

# Include local configuration
BASHRC_LOCAL=~/.bashrc.local
if [ -f $BASHRC_LOCAL ]; then
    source $BASHRC_LOCAL
fi
